irSMHEad
============

=== Personal Usage Warning ===

This is a simple application that scrapes [irshad.az](https://irshad.az/) and sends me the latest items through discord webhook.

---

## Setup
Clone this repo to your desktop and run `npm install` to install all the dependencies

You might want to look into `config.json` to add your own webhook url and add/remove subscriptions.

---

## Usage
Once the dependencies are installed, you can run  `npm start` to start the server. You will then be able to access it at localhost:8000

---

## License
>You can check out the full license [here](https://gitlab.com/leJad/irsmhead/-/blob/master/LICENSE)

This project is licensed under the terms of the **MIT** license.
