const browser = require("./scraper");

const fetchData = async (urls) => {
	return new Promise(function (resolve, reject) {

		// Get product infos from category(or categories) url and push response to "data" array
		const getData = browser
			.GetNewestItems(urls)
			.then( (response) => {return response} );

		// After we are done fetching data send it to the user
		Promise.all([getData]).then((data) => {
			resolve(data);
		});
	});
};

module.exports = {
	fetchData,
};
