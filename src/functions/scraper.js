const puppeteer = require("puppeteer");
const fs = require("fs");

let browser;
let endpoint;

// Launch browser at start
(async () => {
    browser = await puppeteer.launch({
        headless: true,
        args: [
            "--no-sandbox",
            "--disable-setuid-sandbox",
            "--disable-dev-shm-usage",
            "--no-zygote",
            "--disable-accelerated-2d-canvas",
            "--disable-gpu",
            "--no-first-run",
            "--disable-features=AudioServiceOutOfProcess",
            "--single-process",
        ],
    });

    endpoint = browser.wsEndpoint();

    Promise.all([browser]).then(() =>
        console.log("Created a new browser instance")
    );
})();

/**
 * Function to get name, price and url of items in a category
 * @param {string} catalog_url - Url of the category
 * @returns {array} get_product_infos - Name, price and url of the newest items in a category
 **/
async function GetNewestItems(catalog_url) {
    if (catalog_url === null) {
        return { Error: "Url cannot be null!" };
    }

    if (endpoint) {
        // If we already have an instance open don't create a new one and connect to the existing one
        browser = await puppeteer.connect({ browserWSEndpoint: endpoint });
    }

    // Open new page
    const page = await browser.newPage();

    // Block images and css so we can load webpages faster
    await page.setRequestInterception(true);
    page.on("request", (req) => {
        if (
            req.resourceType() === "image" ||
            req.resourceType() === "stylesheet" ||
            req.resourceType() === "font"
        ) {
            req.abort();
        } else {
            req.continue();
        }
    });

    try {
        // Open webpage and wait for it to load
        await page.goto(catalog_url);

        // Wait for selector
        await page.waitForSelector("#all_parts", { timeout: 12000 });
    } catch (err) {
        console.error(err);

        await page.close();
        return { Error: "Invalid url" };
    }
    // Get name and url of products
    let data = await page.$(`
        div#all_parts > 
        section#ProductList > 
        div.container-fluid > 
        div.row.g-0 > 
        div.col-md-9.products__list > 
        div.products__list__body.products__list__body--grid-view > 
        div.product.first > 
        div.product__flex > 
        div.product__flex__left-right
        `);

    // Get price of products
    let price = await page.$(`
        div#all_parts > 
        section#ProductList > 
        div.container-fluid > 
        div.row.g-0 > 
        div.col-md-9.products__list > 
        div.products__list__body.products__list__body--grid-view > 
        div.product.first > 
        div.product__flex-right > 
        div.product__footer
        `);

    try {
        let get_product_prices = await price.evaluate(() => {
            let prices = [];

            // Get prices of products
            let elements = document.getElementsByClassName("product__price");
            for (let element of elements) {
                // The original output should look something like this:
                //          1929.99AZN
                // So we'll use trim function to cut the unwanted spaces at the beginning and end of our string
                let content = element.textContent.trim();

                // Add prices to our array
                prices.push(content);
            }
            return prices;
        });

        // Get product info: name and url
        let get_product_infos = await data.evaluate(async () => {
            let products = [];

            let elements = document.getElementsByClassName("product__name");
            for (let element of elements) {
                // Same usage as the function above
                let content = element.textContent.trim();
                let url = element.getAttribute("href"); // Get url of product
                
                products.push({
                    name: content,
                    price: 0, // We'll change this later with help of "get_product_prices"
                    url: url,
                });
            }
            return products;
        });

        // Collect infos such as name, price and url in get_product_infos array
        for (let i = 0; i < get_product_infos.length; i++) {
            get_product_infos[i].price = get_product_prices[i];
        }

        // Close tab
        await page.close();

        return get_product_infos;
    } catch (err) {
        console.error(err);

        await page.close();
        return { Error: "Invalid url" };
    }
}

module.exports = {
    GetNewestItems,
};
