const axios = require("axios");

const send = async (webhook_url, content) => {
	let readable_content = make_data_readable(content);

	axios(webhook_url, {
		method: "POST",

		headers: {
			"Content-Type": "application/json",
		},
		
		data: {
			content: readable_content,
		},
	});
};

function make_data_readable(data) {
	let content = "";

	data.map((array) => {
		array.map((items) => {
			for (let key in items) {
				content += `${key}: ${items[key]}\n\n`;
			}
		});
	});
	return content;
}

module.exports = {
	send,
};
