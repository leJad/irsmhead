const express = require("express");
const dcWebhook = require("./functions/webhook");
const fetcher = require("./functions/fetcher");

const config = require("../config/config.json");

const cors = require("cors");
const app = express();

const PORT = process.env.PORT || 8000;

app.use(cors("*"));

app.use(express.json());

app.use(
	express.urlencoded({
		extended: true,
	})
);

// Send information to our webhook every 12 hours

setInterval(async () => {
	const urls = config.subscriptions;

	// Send data one by one so we can avoid "content should be fewer than 2000 characters" error
	urls.map((url) => {
		fetcher.fetchData(url).then((data) => {
			dcWebhook.send(config.webhook_url, data);
		});
	});
}, 1000 * 60 * 60 * 12); // 12 hours in milliseconds

app.post("/scrape", async (req, res) => {
	// Get urls from object
	const urls = Object.values(req.body);

	// Send urls and get data
	const getData = urls.map((url) => {
		return fetcher.fetchData(url).then((response) => {
			return response;
		});
	});

	Promise.all(getData).then((data) => {
		const checkForError = Object.keys(data[0][0])[0]

		if (checkForError === "Error") {
			res.status(406).send(data);
		} else {
			res.status(200).send(data);
		}
	});
});

app.listen(PORT, () => console.log(`Server UP on :${PORT}`));
